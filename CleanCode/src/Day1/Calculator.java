package Day1;

import java.util.*;

class Calculator {
    public static double penjumlahan(double result, double number){return result + number;}
    public static double pengurangan(double result, double number){return result - number;}
    public static double pembagian(double result, double number){return result / number;}
    public static double perkalian(double result, double number){return result * number;}

    public static void main(String args[]) {
        Scanner inputScanner = new Scanner(System.in);
        System.out.print("Input number and operators that you want to calculate: ");
        try {
            String inputNumberOperations = inputScanner.next();
            List<String> listNumberOperations= new ArrayList<String>();

            //parsing input menjadi ArrayList berdasarkan angka dan operator
            String parsedUser = "";
            for (int i = 0; i < inputNumberOperations.length(); i++) {
                if(inputNumberOperations.charAt(i) == '+' || inputNumberOperations.charAt(i) == '-' || inputNumberOperations.charAt(i) == '/' || inputNumberOperations.charAt(i) == '*') {
                    listNumberOperations.add(parsedUser);
                    listNumberOperations.add(String.valueOf(inputNumberOperations.charAt(i)));
                    parsedUser = "";
                } else {
                    parsedUser = parsedUser + String.valueOf(inputNumberOperations.charAt(i));
                }
                if (i == inputNumberOperations.length() - 1) {
                    listNumberOperations.add(parsedUser);
                    parsedUser = "";
                }
            }
            //perhitungan
            double number, result = 0;
            for (int i = 0; i < listNumberOperations.size(); i++) {
                if(Objects.equals(listNumberOperations.get(i),"+")){
                    i++;
                    number = Double.parseDouble(listNumberOperations.get(i));
                    result = penjumlahan(result, number);
                } else if (Objects.equals(listNumberOperations.get(i),"-")){
                    i++;
                    number = Double.parseDouble(listNumberOperations.get(i));
                    result = pengurangan(result, number);
                } else if (Objects.equals(listNumberOperations.get(i),"/")){
                    i++;
                    number = Double.parseDouble(listNumberOperations.get(i));
                    result = pembagian(result, number);
                } else if (Objects.equals(listNumberOperations.get(i),"*")){
                    i++;
                    number = Double.parseDouble(listNumberOperations.get(i));
                    result = perkalian(result, number);
                } else result = Double.parseDouble(listNumberOperations.get(i));
            }
            System.out.println("Number Operations Result: " + result);
        } catch (InputMismatchException e) {
            System.out.println("Invalid input");
        }
    }
}